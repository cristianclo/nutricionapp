<?php include("/var/www/html/nutricionapp/header.php") ?>

  <!-- Page Heading -->
  <h1 class="h3 mb-4 text-gray-800">Crear historia</h1>
  <hr>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-md-2">
          <label class="col-md-4 control-label" for="usuario">Seleccionar usuario</label>
          <select id="usuario" name="usuario" class="form-control">
            <option value="-">-----</option>
            <option value="1">Usuario1</option>
            <option value="2">Usuario2</option>
            <option value="3">Usuario3</option>
            <option value="4">Usuario5</option>
          </select>
        </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#basic">Datos básicos</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#medical">Historia médica</a></li>
                <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#nutritional">Historia nutricional</a></li> -->
              </ul>

              
          <div class="tab-content">
            <div class="tab-pane active" id="basic">
                <hr>
                  <form class="form" action="##" method="post" id="registrationForm">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="nombre"><h4>Nombre</h4></label>
                              <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Juana" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="apellido"><h4>Apellido</h4></label>
                              <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Pérez" required>
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4>Teléfono</h4></label>
                              <input type="tel" class="form-control" name="phone" id="phone" placeholder="32112345678" required>
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="direccion"><h4>Dirección</h4></label>
                              <input type="text" class="form-control" name="direccion" id="direccion" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Email</h4></label>
                              <input type="email" class="form-control" name="email" id="email" placeholder="dorreo@cominio.com" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="ocupacion"><h4>Ocupación</h4></label>
                              <input type="text" class="form-control" name="ocupacion" id="ocupacion" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label class="col-md-4 control-label" for="motivoconsulta">Motivo de consulta</label>
                          <div class="col-md-4">                     
                            <textarea class="form-control" id="motivoconsulta" name="motivoconsulta" required></textarea>
                          </div>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label class="col-md-4 control-label" for="peso">Peso</label>  
                              <div class="col-md-2">
                                <input id="peso" name="peso" type="text" placeholder="Kg" class="form-control input-md" required>
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label class="col-md-4 control-label" for="estatura">Estatura</label>  
                              <div class="col-md-2">
                                <input id="estatura" name="estatura" type="text" placeholder="Cm" class="form-control input-md" required>
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Guardar</button>
                                <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reiniciar</button>
                            </div>
                      </div>
                </form>
              
              <hr>
              
             </div><!--/tab-pane-->
             <div class="tab-pane" id="medical">
               
               <h2></h2>
               
               <hr>
                  <form class="form" action="##" method="post" id="registrationForm">
                     
                          
                          <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="presion">Presión arterial alta</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="presion-0">
                            <input type="radio" name="presion" id="presion-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="presion-1">
                            <input type="radio" name="presion" id="presion-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="presion-2">
                            <input type="text" name="presion" id="presion-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="colesterol">Colesterol alto</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="colesterol-0">
                            <input type="radio" name="colesterol" id="colesterol-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="colesterol-1">
                            <input type="radio" name="colesterol" id="colesterol-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="colesterol-2">
                            <input type="text" name="colesterol" id="colesterol-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="cardiovascular">Problemas cardiovasculares</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="cardiovascular-0">
                            <input type="radio" name="cardiovascular" id="cardiovascular-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="cardiovascular-1">
                            <input type="radio" name="cardiovascular" id="cardiovascular-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="cardiovascular-2">
                            <input type="text" name="cardiovascular" id="cardiovascular-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="diabetes">Diabetes tipo II</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="diabetes-0">
                            <input type="radio" name="diabetes" id="diabetes-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="diabetes-1">
                            <input type="radio" name="diabetes" id="diabetes-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="diabetes-2">
                            <input type="text" name="diabetes" id="diabetes-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="sindromemetabolico">Síndrome metabólico</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="sindromemetabolico-0">
                            <input type="radio" name="sindromemetabolico" id="sindromemetabolico-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="sindromemetabolico-1">
                            <input type="radio" name="sindromemetabolico" id="sindromemetabolico-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="sindromemetabolico-2">
                            <input type="text" name="sindromemetabolico" id="sindromemetabolico-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="apnea">Apnea del sueño</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="apnea-0">
                            <input type="radio" name="apnea" id="apnea-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="apnea-1">
                            <input type="radio" name="apnea" id="apnea-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="apnea-2">
                            <input type="text" name="apnea" id="apnea-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="asma">Asma</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="asma-0">
                            <input type="radio" name="asma" id="asma-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="asma-1">
                            <input type="radio" name="asma" id="asma-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="asma-2">
                            <input type="text" name="asma" id="asma-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="artritis">Artritis</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="artritis-0">
                            <input type="radio" name="artritis" id="artritis-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="artritis-1">
                            <input type="radio" name="artritis" id="artritis-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="artritis-2">
                            <input type="text" name="artritis" id="artritis-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="depresion">Depresión</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="depresion-0">
                            <input type="radio" name="depresion" id="depresion-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="depresion-1">
                            <input type="radio" name="depresion" id="depresion-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="depresion-2">
                            <input type="text" name="depresion" id="depresion-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="tiroides">Problemas de tiroides</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="tiroides-0">
                            <input type="radio" name="tiroides" id="tiroides-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="tiroides-1">
                            <input type="radio" name="tiroides" id="tiroides-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="tiroides-2">
                            <input type="text" name="tiroides" id="tiroides-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="desordenes">Desórdenes gastrointestinales</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="desordenes-0">
                            <input type="radio" name="desordenes" id="desordenes-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="desordenes-1">
                            <input type="radio" name="desordenes" id="desordenes-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="desordenes-2">
                            <input type="text" name="desordenes" id="desordenes-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="vesicula">Enfermedad de la vesícula</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="vesicula-0">
                            <input type="radio" name="vesicula" id="vesicula-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="vesicula-1">
                            <input type="radio" name="vesicula" id="vesicula-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="vesicula-2">
                            <input type="text" name="vesicula" id="vesicula-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="renales">Problemas renales</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="renales-0">
                            <input type="radio" name="renales" id="renales-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="renales-1">
                            <input type="radio" name="renales" id="renales-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="renales-2">
                            <input type="text" name="renales" id="renales-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="higado">Problemas de hígado</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="higado-0">
                            <input type="radio" name="higado" id="higado-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="higado-1">
                            <input type="radio" name="higado" id="higado-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="higado-2">
                            <input type="text" name="higado" id="higado-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="marcapasos">Marcapasos</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="marcapasos-0">
                            <input type="radio" name="marcapasos" id="marcapasos-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="marcapasos-1">
                            <input type="radio" name="marcapasos" id="marcapasos-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="marcapasos-2">
                            <input type="text" name="marcapasos" id="marcapasos-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="cancer">Cáncer</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="cancer-0">
                            <input type="radio" name="cancer" id="cancer-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="cancer-1">
                            <input type="radio" name="cancer" id="cancer-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="cancer-2">
                            <input type="text" name="cancer" id="cancer-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="colon">Sindrome del colon irritable</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="colon-0">
                            <input type="radio" name="colon" id="colon-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="colon-1">
                            <input type="radio" name="colon" id="colon-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="colon-2">
                            <input type="text" name="colon" id="colon-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="alimenticios">Desórdenes alimenticios</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="alimenticios-0">
                            <input type="radio" name="alimenticios" id="alimenticios-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="alimenticios-1">
                            <input type="radio" name="alimenticios" id="alimenticios-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="alimenticios-2">
                            <input type="text" name="alimenticios" id="alimenticios-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="estrenimiento">Estreñimiento crónico</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="estrenimiento-0">
                            <input type="radio" name="estrenimiento" id="estrenimiento-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="estrenimiento-1">
                            <input type="radio" name="estrenimiento" id="estrenimiento-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="estrenimiento-2">
                            <input type="text" name="estrenimiento" id="estrenimiento-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="migrana">Migraña</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="migrana-0">
                            <input type="radio" name="migrana" id="migrana-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="migrana-1">
                            <input type="radio" name="migrana" id="migrana-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="migrana-2">
                            <input type="text" name="migrana" id="migrana-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Multiple Radios (inline) -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="celiaca">Enfermedad celiaca/Intolerancia al gluten</label>
                        <div class="col-md-4"> 
                          <label class="radio-inline" for="celiaca-0">
                            <input type="radio" name="celiaca" id="celiaca-0" value="1" checked="checked">
                            Sí
                          </label> 
                          <label class="radio-inline" for="celiaca-1">
                            <input type="radio" name="celiaca" id="celiaca-1" value="2">
                            No
                          </label> 
                          <label class="radio-inline" for="celiaca-2">
                            <input type="text" name="celiaca" id="celiaca-2" >
                            Comentarios
                          </label>
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="medicalotra">Otra</label>  
                        <div class="col-md-4">
                        <input id="medicalotra" name="medicalotra" type="text" placeholder="" class="form-control input-md">
                          
                        </div>
                      </div>

                      <hr>


                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
                </form>
               
             </div><!--/tab-pane-->
             <div class="tab-pane" id="nutritional">
                
                
                  <hr>
                  <form class="form" action="##" method="post" id="registrationForm">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="first_name"><h4>First name</h4></label>
                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4>Last name</h4></label>
                              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4>Teléfono</h4></label>
                              <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h4>Mobile</h4></label>
                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Email</h4></label>
                              <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Location</h4></label>
                              <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="password"><h4>Password</h4></label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="password2"><h4>Verify</h4></label>
                              <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                          </div>
                      </div>
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                            </div>
                      </div>
                </form>
              </div>
               
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
  

<?php include("/var/www/html/nutricionapp/footer.php") ?>