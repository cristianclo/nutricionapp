-- Creando filas en las tablas

CREATE PROCEDURE crear_usuario 
@id_usu int,
@nombre_usu varchar(255),
@contrasena_usu varchar(20)
AS
INSERT INTO usuario (id_usuario, nombre, contrasena)
VALUES (@id_usu, @nombre_usu, @contrasena_usu);

CREATE PROCEDURE crear_historial
@id_hist int,
@nombre_hist varchar(255),
@direccion_hist varchar(100),
@simptomas_hist varchar(1000)
AS
INSERT INTO historial (id_historial, nombre, direccion, simptomas)
VALUES (@id_hist, @nombre_hist, @direccion_hist, @simptomas_hist);

CREATE PROCEDURE crear_usuario_historial
@id_uh int,
@fk1_uh int,
@fk2_uh int
AS
INSERT INTO usuario_historial (id_usuario_historial, id_usuario, id_historial)
VALUES (@id_uh, @fk1_uh, @fk2_uh);

-- Leyendo filas de las tablas

CREATE PROCEDURE leer_usuario @usu int AS
SELECT * FROM usuario WHERE id_usuario = @usu;

CREATE PROCEDURE leer_historial @hist int AS
SELECT * FROM historial WHERE id_historial = @hist;

CREATE PROCEDURE leer_usuario_historial @uh int AS
SELECT * FROM usuario_historial WHERE id_usuario_historial = @uh;

-- Borrando filas de las tablas

CREATE PROCEDURE borrar_usuario @usu int AS
DELETE FROM usuario WHERE id_usuario = @usu;

CREATE PROCEDURE borrar_historial @hist int AS
DELETE FROM historial WHERE id_historial = @hist;

CREATE PROCEDURE borrar_usuario_historial @uh int AS
DELETE FROM usuario_historial WHERE id_usuario_historial = @uh;