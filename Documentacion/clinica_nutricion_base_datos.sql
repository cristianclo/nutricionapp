-- Creacion de tablas

CREATE TABLE usuario (
	id_usuario int PRIMARY KEY,
	nombre varchar(255),
	contrasena varchar(20),
	rol boolean
);

CREATE TABLE historial (
	id_historial int PRIMARY KEY,
	nombre varchar(255),
	direccion varchar(100),
	simptomas varchar(2000)
);

CREATE TABLE usuario_historial (
	id_usuario_historial int PRIMARY KEY,
	id_usuario int,
	id_historial int,
	FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
	FOREIGN KEY (id_historial) REFERENCES historial(id_historial)
);